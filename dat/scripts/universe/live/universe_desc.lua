function generateUniverseDesc()
	return [[The universe is live!

	The automation of the chemical synthesis of DNA. That's the next big aim. What's going to drive synthetic biology is cheap, accurate DNA synthesis. And not just short stretches of DNA known as oligonucleotides, but possibly entire genes. If you can do that with a machine, where you just enter your DNA sequence and the next day you have a piece of DNA 10,000 bases long that you can experiment with, then that will drive the field. We have a big operation aiming to automate DNA synthesis.

How might that transform the field?
I'm sure right now many young people could think of interesting genetic material to design but it's too expensive. If it's cheap and easy you can just keep churning out stuff. It would become trivial to design whole genomes after a while.]]
end